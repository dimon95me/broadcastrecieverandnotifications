package com.sourceit.broadcastrecieverandnotifications;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.widget.Toast;

public class MyReceiver extends BroadcastReceiver {
    private static final String TAG = "MyReceiver";

    @Override
    public void onReceive(Context context, Intent intent) {
        Log.d(TAG, "Airplane Mode: ");
        Toast.makeText(context, "Mode", Toast.LENGTH_SHORT).show();
    }
}
