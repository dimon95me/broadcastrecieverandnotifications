package com.sourceit.broadcastrecieverandnotifications;

import android.app.Service;
import android.content.Intent;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;

public class MyService extends Service {
    public static final String EVENT_KEEP_GOING = "keep_going";

    Handler h = new Handler(new Handler.Callback() {
        @Override
        public boolean handleMessage(Message msg) {
            Intent intent = new Intent(EVENT_KEEP_GOING);
            sendOrderedBroadcast(intent,null);
            h.sendEmptyMessageDelayed(1, 5000);
            return false;
        }
    });

    public MyService() {
    }

    @Override
    public IBinder onBind(Intent intent) {
        // TODO: Return the communication channel to the service.
        throw new UnsupportedOperationException("Not yet implemented");
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {

        h.sendEmptyMessageDelayed(1, 5000);

        return START_STICKY;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        h.removeMessages(1);
    }
}
